# Bitvavo Web Workers 

by Liviu Maftuleac

## Prerequisites

1. For this project we'll need `node` with package manager `npm` or `yarn` 
2. `docker` I'll be using a postgress database inside a container, for the sake of simplicity
3. `pm2` you can install it directly `npm install pm2@latest -g` or `yarn global add pm2`

## Getting Started

1. First we need to have a database, create one using `yarn create:db` from root folder. It will create a container with a postgress database, however you will get stuck in the terminal. 
You can close the process with `ctrl + c` and use `yarn start:db` to restart db container in background.
2. We'll need to run migrations in order to setup the database structure, run `yarn migrate:db`
3. If all went well until now, all we have to do is run our processes, run `yarn start` to start processes via `pm2`
4. Our react application should be available on `http://localhost:3000` 
5. Check status of processes by running `yarn monitor`
6. To end all processes run `yarn stop`

## How it works

1. API is used for receiving and storing each tick from web-workers. Since each tick has a timestamp, I will be using it as a unique identifier as only one of that kind is allowed in the database. 
2. Each WebWorker instance is receiving and sendig ticks to the API. I do not check whether other services has received the same tick, since only one is going to end up in the database. 
3. Frontend application is pooling for the last 10 minutes ticks in the database
4. Each service has a long and short GET `/healthz` and `/healthz/long` check available. This should go into a monitoring service. 
  * `/healthz` is used to check whether the service is available
  * `/healthz/long` is used to check whether the service and it's dependencies are available (ex: API is dependent on database; a webworker is dependent on API)

