module.exports = {
  apps : [{
    name: 'API',
    script: './api/index.js',
    instances: 2,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development',
      INTERN_SECRET: 'rCFL8m0B3uTCihuzvfmf',
      PG_MIN_CONNECTIONS: 2,
      PG_MAX_CONNECTIONS: 10,
      POSTGRES_HOST: 'localhost',
      POSTGRES_DB: 'bitvavo',
      POSTGRES_USER: 'postgres',
      POSTGRES_PASSWORD: 'postgres',
      LOGGER_LEVEL: 'debug',
      PORT: 8080,
    }
  },
  {
    name: 'web-workers',
    script: './workers/index.js',
    instances: 4,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development',
      API_HOST: 'http://localhost:8080/api/v1',
      API_SECRET: 'rCFL8m0B3uTCihuzvfmf', // must be same as API INTERN_SECRET
      LOGGER_LEVEL: 'debug',
      PORT: 3030,
    }
  },
  {
    name: 'frontend',
    script: 'cd frontend && npm start',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development',
      REACT_APP_API_HOST: 'http://localhost:8080/api/v1/'
    }
  }
]
};
