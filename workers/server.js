const express = require('express');
const helmet = require('helmet')
const cors = require('cors');
const bodyParser = require('body-parser');

const expressLogger = require('./loggers/expressLogger');
const { PORT } = require('./config.env');
const healthzRouter = require('./routes/healthz');
const bitvavo = require('./data-source/bitvavo');

class Server {
  constructor(readyCallback) {
    this.expressServer = express();
    this.expressServer.use(helmet());
    this.expressServer.disable('x-powered-by');
    this.expressServer.use(cors());
    this.expressServer.use(bodyParser.json());
    this.expressServer.use(bodyParser.urlencoded({ extended: true }));
    this.expressServer.use(expressLogger());
    this.expressServer.use('/', healthzRouter);
    const port = PORT || 3000;
    this.expressServer.listen(port, readyCallback);
    bitvavo.startWatcher();
    console.log(`Listening to ${port}`);
  }
}

module.exports = Server;
