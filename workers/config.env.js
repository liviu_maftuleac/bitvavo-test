const {
  API_HOST,
  API_SECRET,
  LOGGER_LEVEL,
  NODE_ENV,
  PORT,
} = process.env;

module.exports = {
  API_HOST: API_HOST || 'http://localhost:8080/api/v1',
  API_SECRET: API_SECRET || 'secret-password',
  LOGGER_LEVEL,
  NODE_ENV,
  PORT
};
