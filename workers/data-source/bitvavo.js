const bitvavo = require('bitvavo')().options({});
const ApiService = require('../services/api-service');

class BitvavoWorker {
  static async startWatcher() {
    bitvavo.websocket.subscriptionTicker24h('BTC-EUR', (tick) => {
      console.log('Received Tick', tick.timestamp)
      ApiService.pushTick(tick);
    });
  }

  static async checkHealthz() {
    await bitvavo.websocket.checkSocket()
  }

}

module.exports = BitvavoWorker;

