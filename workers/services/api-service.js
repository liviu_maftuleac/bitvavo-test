const request = require('request-promise-native');
const { API_HOST, API_SECRET } = require('../config.env');


const defaultHeaders = {
  Authorization: `Internal ${API_SECRET}`,
}

class ApiService {
  constructor() {
    this.api = `${API_HOST}/`;
  }

  async pushTick(tick) {
    try {
      const res = await request({
        method: 'POST',
        json: true,
        jsonReviver: true,
        body: tick,
        headers: defaultHeaders,
        url: `${this.api}/data/tick`,
      })

      return res;
    } catch (error) {
      console.log('UserService::validateToken() error:', error);
    }
  }

}

module.exports = new ApiService();