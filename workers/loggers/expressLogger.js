const winston = require('winston')
const expressWinston = require('express-winston')
const { format } = require('winston')
const { combine, printf, timestamp, splat } = format

const getDate = d => {
  var date = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate()
  var time = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds()
  return date + ' ' + time
}

const customFormat = printf(info => {
  return `{ "timestamp":"${getDate(new Date(info.timestamp))}",\
  "level":"${info.level}", \
  "method":"${info.meta.req.method}", \
  "url":"${info.meta.req.url}", \
  "statusCode":"${info.meta.res.statusCode}", \
  "message":"${info.meta.res.body ? info.meta.res.body.message || '' : ''}", \
  "responseTime":"${info.meta.responseTime}ms" }`
})

module.exports = () =>
  expressWinston.logger({
    transports: [new winston.transports.Console()],
    responseWhitelist: [...expressWinston.responseWhitelist, 'body'],
    format: combine(splat(), timestamp(), customFormat),
    level: (req, res) => {
      var level = ''
      if (res.statusCode >= 100) {
        level = 'info'
      }
      if (res.statusCode >= 400) {
        level = 'warn'
      }
      if (res.statusCode >= 500) {
        level = 'error'
      }
      return level
    }
  })
