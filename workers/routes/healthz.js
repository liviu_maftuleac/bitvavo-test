const { Router } = require('express');
const bitvavo = require('../data-source/bitvavo');
const HttpStatus = require('http-status-codes');
const healthzRouter = new Router();


healthzRouter.get('/healthz', (req, res) => {
  res.status(HttpStatus.OK).json({ status: 'OK' });
});

healthzRouter.get('/healthz/long', async (req, res) => {
  try {
    const status = await bitvavo.checkHealthz();
    res.status(HttpStatus.OK).json({status: 'OK'});
  } catch(err) {
    res.status(HttpStatus.SERVICE_UNAVAILABLE);
  }
});

module.exports = healthzRouter;