const HttpStatus = require('http-status-codes');
const { INTERN_SECRET } = require('../config.env');

module.exports = (req, res, next) => {
  const { headers } = req;
  
  if (!headers.authorization) {
    return res.status(HttpStatus.UNAUTHORIZED).json({error: 'UNAUTHORIZED' })
  }

  const match = headers.authorization.match(/^Internal (.*)$/);
  if (!match) {
    return res.status(HttpStatus.UNAUTHORIZED).json({error: 'UNAUTHORIZED' })
  }

  const token = match[1];
  if (token === INTERN_SECRET) {
    return next();
  }

  return res.status(HttpStatus.UNAUTHORIZED).json({error: 'UNAUTHORIZED' })
}