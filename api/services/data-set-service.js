const db = require('../db/connection').knex;
const { BITVAVO_DATA } = require('../db/tableNames');
const HttpStatus = require('http-status-codes');

class DataSetService  {

  static async registerDataSet(req, res) {
    try {
      const tick = req.body;
      const q = `INSERT INTO ${BITVAVO_DATA} (
        market,
        open,
        high,
        low,
        last,
        volume,
        volume_quote,
        bid,
        bid_size,
        ask,
        ask_size,
        timestamp
      )
         VALUES(
          '${tick.market}',
          '${tick.open}',
          '${tick.high}',
          '${tick.low}',
          '${tick.last}',
          '${tick.volume}',
          '${tick.volumeQuote}',
          '${tick.bid}',
          '${tick.bidSize}',
          '${tick.ask}',
          '${tick.askSize}',
          ${tick.timestamp}
         ) ON CONFLICT (timestamp) DO NOTHING;`

      await db.raw(q);
      res.status(200).json(tick);
    } catch (err) {
      console.log('DataSetService>registerDataSet :: REGISTER TICK ERROR :', err);
      res.status(HttpStatus.SERVICE_UNAVAILABLE).json({error: 'REGISTER TICK ERROR'});
    }
  }

  static async getDataSet(req, res) {
    try { 
    const currentTime = new Date().getTime();
    const tenMinutes = 1000 * 60 * 10;
    const oneHourDiff = currentTime - tenMinutes;

    const results = await db
      .select('*')
      .from(BITVAVO_DATA)
      .whereBetween('timestamp', [oneHourDiff, currentTime])
      .orderBy('timestamp', 'asc');

    res.status(200).json(results);
    } catch(err) {
      console.log('DataSetService>getDataSet :: CANNOT GET TICKS SET :', err);
      res.status(HttpStatus.SERVICE_UNAVAILABLE).json({error: 'GET TICKS SET ERROR'});
    }
  }

  static async getDataSetByTimestamp(req, res) {
    try {
    const { timestamp } =  req.params;
    const results = await db
      .select('*')
      .from(BITVAVO_DATA)
      .where('timestamp', timestamp)
      .then(t => t[0]);

    res.status(200).json(results);
    } catch(err) {
      console.log('DataSetService>getDataSetByTimestamp :: CANNOT GET TICK :', err);
      res.status(HttpStatus.SERVICE_UNAVAILABLE).json({error: 'GET TICK ERROR'});
    }
  }

}

module.exports = DataSetService;