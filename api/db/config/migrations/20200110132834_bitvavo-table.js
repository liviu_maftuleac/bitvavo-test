const { BITVAVO_DATA } = require('../../tableNames');

exports.up = async (knex) => {
  await knex.schema.createTableIfNotExists(BITVAVO_DATA, (table) => {
    // table.increments().primary();
    table.bigInteger('timestamp').primary();
    table.string('market').notNullable();
    table.string('open').notNullable();
    table.string('high').notNullable();
    table.string('low').notNullable();
    table.string('last').notNullable();
    table.string('volume').notNullable();
    table.string('volume_quote').notNullable();
    table.string('bid').notNullable();
    table.string('ask').notNullable();
    table.string('bid_size').notNullable();
    table.string('ask_size').notNullable();
  });
};

exports.down = async (knex) => {
  await knex.schema.dropTableIfExists(BITVAVO_DATA);
};
