const CONFIG_ENV = require('../../config.env');
const {
  PG_MIN_CONNECTIONS,
  PG_MAX_CONNECTIONS,
  POSTGRES_HOST,
  POSTGRES_DB,
  POSTGRES_USER,
  POSTGRES_PASSWORD,
} = CONFIG_ENV;

const MIN_CONNECTIONS = parseInt(PG_MIN_CONNECTIONS, 10) || 2;
const MAX_CONNECTIONS = parseInt(PG_MAX_CONNECTIONS, 10) || 10;

module.exports = {
  local: {
    client: 'postgresql',
    debug: true,
    connection: {
      host:     'localhost',
      database: 'bitvavo',
      user:     'postgres',
      password: 'postgres',
    },
    acquireConnectionTimeout: 100000,
    migrations: {
      tableName: 'migrations',
      directory: './migrations',
    },
    seeds: {
      directory: './seeds',
    },
  },
  development: {
    client: 'postgresql',
    debug: false,
    connection: {
      host:     POSTGRES_HOST,
      database: POSTGRES_DB,
      user:     POSTGRES_USER,
      password: POSTGRES_PASSWORD,
    },
    acquireConnectionTimeout: 100000,
    migrations: {
      tableName: 'migrations',
      directory: './migrations',
    },
    seeds: {
      directory: './seeds',
    },
    pool: { min: MIN_CONNECTIONS, max: MAX_CONNECTIONS },
  },
};
