const knex = require('knex');
const { NODE_ENV } = require('../config.env');
const knexConfig = require('./config/knexfile');

class DatabaseConnection {
  constructor() {
    const config = knexConfig[NODE_ENV || 'local'];
    this.knex = knex(config);
  }
}

module.exports = new DatabaseConnection();
