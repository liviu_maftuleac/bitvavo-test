const {
  INTERN_SECRET,
  PG_MIN_CONNECTIONS,
  PG_MAX_CONNECTIONS,
  POSTGRES_HOST,
  POSTGRES_DB,
  POSTGRES_USER,
  POSTGRES_PASSWORD,
  LOGGER_LEVEL,
  NODE_ENV,
  PORT,
} = process.env;

module.exports = {
  INTERN_SECRET : INTERN_SECRET || 'secret-password',
  PG_MIN_CONNECTIONS,
  PG_MAX_CONNECTIONS,
  POSTGRES_HOST,
  POSTGRES_DB,
  POSTGRES_USER,
  POSTGRES_PASSWORD,
  LOGGER_LEVEL,
  NODE_ENV,
  PORT
};
