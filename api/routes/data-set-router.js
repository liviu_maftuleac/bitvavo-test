const { Router } = require('express');
const internAuth = require('../middleware/internal-auth');
const DataSertService = require('../services/data-set-service');

const router = new Router();

router.post('/tick', internAuth, DataSertService.registerDataSet);
router.get('/tick', DataSertService.getDataSet);
router.get('/tick/:timestamp', DataSertService.getDataSetByTimestamp);

module.exports = router;
