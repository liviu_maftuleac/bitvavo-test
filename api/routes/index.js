const { Router } = require('express');
const dataSetRouter = require('./data-set-router');

const router = new Router();

router.use('/data', dataSetRouter);

module.exports = router;
