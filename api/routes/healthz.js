const { Router } = require('express');
const HttpStatus = require('http-status-codes');
const db = require('../db/connection').knex;

const healthzRouter = new Router();

healthzRouter.get('/healthz', (req, res) => {
  res.status(HttpStatus.OK).json({ status: 'OK' });
});

healthzRouter.get('/healthz/long', async (req, res) => {
  try {
    await db.raw('select 1');
    res.status(HttpStatus.OK).json({ status: 'OK' });
  } catch(error) {
    console.log('Long Heathz Error', error);
    res.status(HttpStatus.SERVICE_UNAVAILABLE).json({error: 'LONG HEALTHZ ERROR'});
  }
});

module.exports = healthzRouter;