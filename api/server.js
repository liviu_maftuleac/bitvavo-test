const express = require('express');
const helmet = require('helmet')
const cors = require('cors');
const bodyParser = require('body-parser');

const DatabaseConnection = require('./db/connection');
const expressLogger = require('./loggers/expressLogger');
const { PORT } = require('./config.env');
const healthzRouter = require('./routes/healthz');
const routes = require('./routes');

class Server {
  constructor(readyCallback) {
    this.expressServer = express();
    this.expressServer.use(helmet());
    this.expressServer.disable('x-powered-by');
    this.expressServer.use(cors());
    this.expressServer.use(bodyParser.json());
    this.expressServer.use(bodyParser.urlencoded({ extended: true }));
    this.expressServer.use(expressLogger());
    this.expressServer.use('/', healthzRouter);
    this.expressServer.use('/api/v1', routes);
    const port = PORT || 8080;
    this.expressServer.listen(port, readyCallback);
    console.log(`Listening to ${port}`);
    this.databaseConnection = DatabaseConnection;
  }
}

module.exports = Server;
