import React from 'react'
import './App.css'
import MyChart from './components/chart'

function App () {
  return (
    <div className='App'>
      <nav>
        <div className='nav-wrapper blue lighten-5'>
          <a href='#' className='brand-logo'>
            <img className="responsive-img" src='https://bitvavo.com/assets/img/logo/logo-dark.svg' />
          </a>
        </div>
      </nav>
      <div className='container'>
        <h3 className="center-align">Bitvavo API integration</h3>
        <MyChart />
      </div>
    </div>
  )
}

export default App
