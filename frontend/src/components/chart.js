import React, { PureComponent } from 'react';
import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer
} from 'recharts';

import ApiService from '../services/api-service';

export default class Example extends PureComponent {

  constructor() {
    super();
    this.timer = null;
    this.state = {
      pooling: true,
      data: []
    }
  }

  componentDidMount() {
    this.getChartData();
    this.startPooling();
  }

  startPooling() {
    this.timer = setInterval(async () => {
      await this.getChartData();
    }, 1000);
    this.setState({pooling: true})
  }

  stopPooling() {
    clearInterval(this.timer);
    this.setState({pooling: false});
  }

  toggle() {
    if (this.state.pooling) {
      this.stopPooling();
    } else {
      this.startPooling();
    }
  }

  async getChartData() {
    const data = await ApiService.getTicks();

    if (data) {
      const fineData = data.map(t=>({
        bid : parseFloat(t.bid),
        time : new Date(parseInt(t.timestamp)).toLocaleString().split(',')[1]
      }))
      this.setState({data: fineData});
    }
  }

  render() {
    const { data, pooling } = this.state;
    return (
      <div>
        <ResponsiveContainer width="100%" aspect={2} >
        <LineChart
          data={data}
          margin={{
            top: 5, right: 30, left: 20, bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <YAxis type="number" domain={['dataMin', 'dataMax']} />
          <XAxis dataKey="time" interval={25} />
          <Tooltip />
          <Legend />
          <Line isAnimationActive={false} type="linear" dataKey="bid" name="BTC-EUR" stroke="#82ca9d" />
        </LineChart>
        </ResponsiveContainer>
          <div className="row">
            <div className="col s6"/>
            <div className="col s6">
                <div className="row">
                  <div className="col s8 right-align"><label>Data pooling </label></div>
                  <div className="col s4">
                    <div className="switch">
                      <label>
                        Off
                        <input type="checkbox" value={pooling} defaultChecked={pooling} onClick={()=>{this.toggle()}}/>
                        <span className="lever"></span>
                        On
                      </label>
                    </div>
                  </div>
                </div>
            </div>
          </div>
      </div>
    );
  }
}
