export default function httpRequest(url, opts) {
  
  return new Promise(
     (resolve, reject) => {
      fetch(url, opts).then(
        (response) => {
          const res = response.json();
          res.then((r)=>{
            if (response.ok) {
              console.log('REQUEST OK', r.data);
              resolve(res);
            } else {
              console.log(`BAD Request on ${url} :: ${JSON.stringify(response)}`);
              reject({code: r.code, message: r.message});
            }
          })
        })
      .catch(
        (error) => {
          reject(console.log(`Unable to reach API. ${error.message} ${error.code}`));
        }
      );
    }
  ); 
}