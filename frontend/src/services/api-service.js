import httpRequest from './http-request';
import { API_HOST } from '../config';


export default class ApiService {
  static async getTicks() {
    try {
      const res = await httpRequest(`${API_HOST}/data/tick/`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        },
      });
      return res;
    } catch (error) {
      console.log('API Service error', error);
    }
  }
}
