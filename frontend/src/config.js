const API_HOST = process.env.REACT_APP_API_HOST || 'http://localhost:8080/api/v1/'

export {
  API_HOST
}
